from django.db import models
from django.conf import settings


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    # foreign key
    # User model
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )
    # category_one = ExpensesCategory("name_one","account_one")


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
    )


# foreign key
# User model
# related name of "account"
# cascaded deletion relation. something = models.CASCADE
# class Book(models.Model):

# EXAMPLE TEMPLATE
# title = models.CharField(max_length=200)
# summary = models.TextField(max_length=1000)
# isbn = models.CharField(max_length=13, unique=True)

# author = models.ForeignKey(
#
#     'Author',
#     related_name="books",
#     on_delete=models.CASCADE,
# )


class Receipt(models.Model):
    # decimalField
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    # reserch how to use

    tax = models.DecimalField(max_digits=10, decimal_places=3)
    # research how to use

    date = models.DateTimeField(auto_now_add=True)
    # research how to use

    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    # foreign key, related naem of reciepts,
    #  CASCADE relation

    category = models.ForeignKey(
        ExpenseCategory,
        related_name="receipts",
        on_delete=models.CASCADE,
    )

    # foreign key to ExpensesCategory model
    # related name of receipts,
    # CASCADE relation

    account = models.ForeignKey(
        Account,
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )


# receipt_one = Receipt("vendor_one", "40.00", .1", "01-12-12", "purchaser_one",
# "category_one","account_one" )
# receipt_one.vendor


# foriegn key to Account model
# related name of "receipts"
# CASCADE relation
# allowed to be null=True
